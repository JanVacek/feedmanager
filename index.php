<?php
require 'FeedManager.php';
$feedManager = new FeedManager();

if(isset($_POST["download"])){ // form submitted
    if($_POST['feed-format'] == "xml") // from XML or JSON feed ?
        $feedManager->saveFeedToJson(false);
    else
        $feedManager->saveFeedToJson(true);
    echo("Feed parsed!");
}

?>

<!DOCTYPE html>
<html>
<head>
    <title>ss</title>
</head>
<body>

<h1>Choose feed format and download parsed json</h1>
<form method="post">
    <label for="xml">XML</label>
    <input type="radio" id="xml" name="feed-format" value="xml" checked>
    <label for="json">JSON</label>
    <input type="radio" id="json" name="feed-format" value="json">
    <input type="submit" name="download" value="Download">
</form>

<a href="result.json">See the result</a>

</body>
</html>
