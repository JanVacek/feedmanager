<?php

class FeedManager
{

    const API_KEY = "4ccf2f6591e344ee74d12e1491135c07";

    /**
     * @return object
     */
    private function getJsonFeed(): object
    {
        $feed = "https://api.themoviedb.org/3/movie/popular?api_key=".self::API_KEY."&language=en-US&page=1";
        $feed = json_decode(file_get_contents($feed));
        return $feed;
    }


    /**
     * @return SimpleXMLElement|string
     */
    private function getXmlFeed()
    {
        $feed = "http://link.brightcove.com/services/mrss/player835199013001/835233035001/new";
        $feed = simplexml_load_file($feed);
        return $feed;

    }

    /**
     * @param bool $isJson
     */
    public function saveFeedToJson(bool $isJson): void
    {
        $result = [];

        if($isJson){
            $iterable_json = $this->getJsonFeed()->results; // get JSON feed
            foreach($iterable_json as $item) //process every item attributes and pick just the wanted ones
            {
                $images = array(
                    "poster_path"=>$item->poster_path,
                    "backdrop_path"=>$item->backdrop_path
                );
                $genres = [];
                foreach($item->genre_ids as $genre_item)
                {
                    array_push($genres,$genre_item);
                }
                array_push($result,array( //make an array of values for one item of JSON
                    "title"=>(string)$item->title,
                    "description"=>$item->overview,
                    "images"=>$images,
                    "genres"=>$genres,
                    "releaseDate"=>strtotime($item->release_date) // UNIX timestamp
                ));
            }
        }else{
            $xml_feed = $this->getXmlFeed();
            $items = $xml_feed->channel->item;
            foreach($items as $item)//process every item attributes and pick just the wanted ones
            {
                $media = $item->children("http://search.yahoo.com/mrss/"); // accesss to <media:*>
                $images = [];
                foreach($media->thumbnail as $thumb)
                {
                    array_push($images,(string)$thumb->attributes()['url']);
                }
                array_push($result,array( //make an array of values for one item of JSON
                    "title"=>(string)$item->title,
                    "description"=>(string)$item->description,
                    "images"=>$images,
                    "genres"=>explode(",",$media->keywords), // from string separated by ',' to an array
                    "releaseDate"=>strtotime($item->pubDate) // UNIX timestamp
                ));
            }
        }
        file_put_contents("result.json",json_encode($result, JSON_PRETTY_PRINT)); // make final JSON and save to 'result.json'
    }
}
